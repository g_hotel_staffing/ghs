﻿app.service("angularService", function ($http) {

    //get All Eployee
    this.getEmployees = function () {
        return $http.get("/api/JobSeekerService");
    };

    // get Employee By Id
    this.getEmployee = function (employeeID) {

        var response = $http({
            method: "get",
            url: "/api/JobSeekerService",
            params: {
                id: employeeID
            }

        });
        return response;
    }

    // Update Employee 
    this.updateEmp = function (JobSeeker) {

         var response = $http.post('/api/JobSeekerService', JobSeeker);

        //var response = $http({
        //    method: "post",
        //    url: "/api/JobSeekerService",
        //    data: { JobSeeker: JobSeeker },
        //    dataType: "json"
        //});
        return response;
    }
    //this.updateEmp = function (JobSeeker) {

    //    var response = $http.post('/api/JobSeekerService', JobSeeker);

    //    return response;
    //}


});
﻿app.controller("myCntrl", function ($scope, angularService) {
    $scope.divEmployee = false;

    $scope.rowIndex = false;

    GetAllEmployee();


    function GetAllEmployee() {

        var getData = angularService.getEmployees();

        getData.then(function (emp) {
            $scope.employees = emp.data;

        }, function () {
            alert('Error in getting records');
        });
    }


    function Reset() {
        $scope.Feedback = "";
        $scope.IsActive = "";
        $scope.selectedOption = 0;
    }

    $scope.editEmployee = function (employee) {
        var getData = angularService.getEmployee(employee.Id);
        getData.then(function (emp) {
            $scope.employee = emp.data;
            $scope.Id = employee.Id;
            $scope.Name = employee.Name;
            $scope.Feedback = employee.Feedback;
            $scope.Rating = employee.Rating;
            $scope.IsActive = employee.IsActive;
            $scope.IsInterview = employee.IsInterview;
            $scope.options = [{ name: "Rating", id: 0 }, { name: "1", id: 1 }, { name: "2", id: 2 }, { name: "3", id: 3 }, { name: "4", id: 4 }, { name: "5", id: 5 }];
            $scope.selectedOption = $scope.options[$scope.Rating];

        }, function () {
            alert('Error in getting records');
        });
    }

    $scope.UpdateEmployee = function () {
        var Employee = {
            Feedback: $scope.Feedback,
            Rating: $scope.selectedOption.id,
            IsActive: $scope.IsActive,
            IsInterview: $scope.IsInterview

        };

        Employee.Id = $scope.Id;
        var getData = angularService.updateEmp(Employee);
        getData.then(function (msg) {
            GetAllEmployee();
            alert('Updated');
            Reset();
            $scope.divEmployee = false;
        }, function () {
            alert('Error in updating record');
        });

    }



    $scope.RegisterJobSeeker = function () {

        if (!$scope.registerform.$valid) {
            return false;
        }
        var JobSeeker = {
            Name: $scope.Name,
            Email: $scope.Email,
            Position: $scope.Position,
            Mobile: $scope.Mobile,
            Salary: $scope.Salary,
            Typeofposition: $scope.Typeofposition,
            workplace: $scope.workplace,
            File: $scope.File
        };
        var getData = angularService.updateEmp(JobSeeker);
    }

  



});
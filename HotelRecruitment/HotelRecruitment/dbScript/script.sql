ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Roles]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/30/2016 7:55:15 PM ******/
DROP TABLE [dbo].[User]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/30/2016 7:55:15 PM ******/
DROP TABLE [dbo].[Roles]
GO
/****** Object:  Table [dbo].[JobSeeker]    Script Date: 5/30/2016 7:55:15 PM ******/
DROP TABLE [dbo].[JobSeeker]
GO
/****** Object:  Table [dbo].[JobSeeker]    Script Date: 5/30/2016 7:55:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobSeeker](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Mobile] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[Salary] [decimal](18, 2) NULL,
	[Typeofposition] [nvarchar](50) NULL,
	[workplace] [nvarchar](50) NULL,
	[Position] [nvarchar](50) NULL,
	[CV] [varchar](200) NULL,
	[Feedback] [varchar](500) NULL,
	[Rating] [int] NULL,
	[IsInterview] [bit] NULL,
 CONSTRAINT [PK_JobSeeker] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/30/2016 7:55:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/30/2016 7:55:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Mobile] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](200) NULL,
	[RoleId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[JobSeeker] ON 

INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (1, N'sunil', N'9988392529', N'simfdsa@fad.in', 0, CAST(0x0000A60F016E7D7C AS DateTime), 0, CAST(1000.00 AS Decimal(18, 2)), N'position1', N'postion', N'kljldkfjaslfkd', N'635996384618903601Test.pdf', NULL, NULL, NULL)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (2, N'jlfkjgfl', N'fggfds654654', N'fdasfda2@fds.in', 0, CAST(0x0000A60F016F218C AS DateTime), 0, CAST(10000.00 AS Decimal(18, 2)), N'klj', N'fdafd', N'dfasdf', N'635996387271807372Test.pdf', N'test', 4, 0)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (3, N'sunil singh', N'9988392529', N'Sini@dasfd.in', 0, CAST(0x0000A610009C8010 AS DateTime), 0, CAST(1500.00 AS Decimal(18, 2)), N'fkldjsalkj', N'lkjldsfkjal', N'jlkfjdalsfd', N'635996789885653940images.pdf', N'fd', 3, 1)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (4, N'rohit katoch', N'9988392529', N'rohit@technocodz.com', 0, CAST(0x0000A61000A2F2C9 AS DateTime), 0, CAST(1500000.00 AS Decimal(18, 2)), N'software enginner', N'mohali', N'se', N'635996803966178283images.pdf', N'comments', 4, 1)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (5, N'fdlkasjl', N'fdaslkjl', N'lkfjdasl@fsda.fin', 0, CAST(0x0000A610017DE2F2 AS DateTime), 0, CAST(1500.00 AS Decimal(18, 2)), N'lkjdflaskjfl', N'ljkfdaslfkjds', N'ldfkajlkjl', N'635997282243814391images.pdf', N'comments', 4, 1)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (1001, N'test', N'fdsafd', N'test@fasd.in', 1, CAST(0x0000A61600866735 AS DateTime), 0, CAST(10000.00 AS Decimal(18, 2)), N'dfasfd', N'dsfafd', N'fjdalsfjd', N'636001925608168665Djago FULL REQUIREMENTS.pdf', N'fdsa', 1, 1)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (1002, N'fdasf', N'fdas', N'fdssa', 1, CAST(0x0000A61600922D7C AS DateTime), 0, CAST(10.00 AS Decimal(18, 2)), N'dfas', N'fdasaf', N'fdsa', N'636001951336887214Djago FULL REQUIREMENTS.pdf', N'fs', 2, 1)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (1003, N'Rohit Katoch', N'09988392529', N'rohit@gmail.com', 1, CAST(0x0000A61600A03034 AS DateTime), 0, CAST(100000.00 AS Decimal(18, 2)), N'software developer', N'anywhere', N'se', N'636001981936664718Djago FULL REQUIREMENTS.pdf', N'feedback', 2, 0)
INSERT [dbo].[JobSeeker] ([Id], [Name], [Mobile], [Email], [IsActive], [CreatedOn], [IsDeleted], [Salary], [Typeofposition], [workplace], [Position], [CV], [Feedback], [Rating], [IsInterview]) VALUES (1004, NULL, NULL, N'fddfsf@dfas.in', 0, CAST(0x0000A61600CBD1D3 AS DateTime), 0, CAST(12000.00 AS Decimal(18, 2)), N'dsa', N'fds', N'fdas', N'636002077250771057Djago FULL REQUIREMENTS.pdf', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[JobSeeker] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name], [IsActive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[Roles] ([Id], [Name], [IsActive]) VALUES (2, N'Job seeker', 1)
INSERT [dbo].[Roles] ([Id], [Name], [IsActive]) VALUES (3, N'Hotel Owner', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Name], [Mobile], [Email], [Password], [RoleId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Admin', N'09988392529', N'admin@gmail.com', N'JNGuicG0l9amrBO1wj1MuA==', 1, CAST(0x0000A61600824E71 AS DateTime), CAST(0x0000A61600824E71 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Roles]
GO

﻿using DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace HotelRecruitment.Controllers
{
    public class JobSeekerRegistrationServiceController : ApiController
    {
        private static readonly string ServerUploadFolder = "C:\\Temp"; //Path.GetTempPath();
        HotelManagementEntities dbContext = new HotelManagementEntities();
        // GET api/jobseeker1
        public IEnumerable<JobSeeker> Get()
        {
            return dbContext.JobSeekers.Where(x => x.IsDeleted == false).ToList().OrderByDescending(x => x.CreatedOn);
        }

        public void Post()
        {

            DAL.JobSeeker model = new JobSeeker();

            model.Name = HttpContext.Current.Request.Form["Name"];
            model.Email = HttpContext.Current.Request.Form["Email"];
            model.Position = HttpContext.Current.Request.Form["Position"];
            model.Mobile = HttpContext.Current.Request.Form["Mobile"];

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form["Salary"]))
            {
                model.Salary = Convert.ToDecimal(HttpContext.Current.Request.Form["Salary"]);
            }

            model.Typeofposition = HttpContext.Current.Request.Form["Typeofposition"];
            model.workplace = HttpContext.Current.Request.Form["workplace"];
            model.CreatedOn = DateTime.Now;
            model.IsActive = false;
            model.IsDeleted = false;
            model.IsInterview = false;

            if (HttpContext.Current.Request.Files.Count > 0)
            {
                var fileName = Path.GetFileName(HttpContext.Current.Request.Files[0].FileName);

                string fileExtension = Path.GetExtension(fileName);
                fileName = DateTime.Now.Ticks + fileName;
                var path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Upload/CV/"), fileName);
                HttpContext.Current.Request.Files[0].SaveAs(path);
                model.CV = fileName.ToString();
            }
            dbContext.JobSeekers.Add(model);
            dbContext.SaveChanges();
        }

        // GET api/jobseeker1/5
        public JobSeeker Get(int id)
        {
            return dbContext.JobSeekers.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/jobseeker1
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/jobseeker1/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/jobseeker1/5
        public void Delete(int id)
        {
        }
    }
}

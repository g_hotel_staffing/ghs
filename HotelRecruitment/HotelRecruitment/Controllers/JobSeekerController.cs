﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotelRecruitment.Controllers
{
    public class JobSeekerController : Controller
    {
        HotelManagementEntities dbContext = new HotelManagementEntities();
        //
        // GET: /JobSeeker/
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registration(JobSeeker model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DAL.JobSeeker jobSeeker = new DAL.JobSeeker();

                    jobSeeker.Name = model.Name;

                    jobSeeker.Email = model.Email;
                    jobSeeker.Mobile = model.Mobile;
                    jobSeeker.Salary = model.Salary;
                    jobSeeker.Typeofposition = model.Typeofposition;
                    jobSeeker.Position = model.Position;
                    jobSeeker.workplace = model.workplace;
                    jobSeeker.IsActive = false;
                    jobSeeker.IsDeleted = false;
                    jobSeeker.CreatedOn = DateTime.Now;


                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);

                        string fileExtension = Path.GetExtension(fileName);

                        if (fileExtension != ".pdf")
                        {
                            return View();
                        }
                        fileName = DateTime.Now.Ticks + fileName;

                        var path = Path.Combine(Server.MapPath("~/Content/Upload/CV/"), fileName);
                        file.SaveAs(path);
                        jobSeeker.CV = fileName.ToString();
                    }

                    dbContext.JobSeekers.Add(jobSeeker);
                    dbContext.SaveChanges();

                }

                catch (DbEntityValidationException e)
                {

                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw e;

                }

            }

            return RedirectToAction("Thankyou");
        }

        public ActionResult Update(int Id)
        {

            DAL.JobSeeker jobSeeker = dbContext.JobSeekers.Where(x => x.Id == Id).FirstOrDefault();
            Model.JobSeeker model = new Model.JobSeeker();
            model.Id = jobSeeker.Id;
            model.Name = jobSeeker.Name;
            model.Mobile = jobSeeker.Mobile;
            model.Email = jobSeeker.Email;
            model.Salary = jobSeeker.Salary;
            model.Typeofposition = jobSeeker.Typeofposition;
            model.workplace = jobSeeker.workplace;
            model.Position = jobSeeker.Position;
            model.CreatedOn = jobSeeker.CreatedOn;
            model.CV = jobSeeker.CV;
            model.IsActive = jobSeeker.IsActive;
            model.IsDeleted = jobSeeker.IsDeleted;

            return View(model);
        }
        [HttpPost]
        public ActionResult Update(JobSeeker model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    //DAL.JobSeeker jobSeeker = new DAL.JobSeeker();
                    //jobSeeker.Name = model.Name;
                    //jobSeeker.Email = model.Email;
                    //jobSeeker.Mobile = model.Mobile;
                    //jobSeeker.Salary = model.Salary;
                    //jobSeeker.Typeofposition = model.Typeofposition;
                    //jobSeeker.Position = model.Position;
                    //jobSeeker.workplace = model.workplace;
                    //jobSeeker.IsActive = true;
                    //jobSeeker.IsDeleted = false;
                    //jobSeeker.CreatedOn = DateTime.Now;

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);

                        string fileExtension = Path.GetExtension(fileName);

                        if (fileExtension != ".pdf")
                        {
                            return View();
                        }
                        fileName = DateTime.Now.Ticks + fileName;

                        var path = Path.Combine(Server.MapPath("~/Content/Upload/CV/"), fileName);
                        file.SaveAs(path);
                        model.CV = fileName.ToString();
                    }


                    dbContext.Entry(model).State = EntityState.Modified;
                    dbContext.SaveChanges();

                }

                catch (Exception ex)
                {

                    throw ex;

                    //foreach (var eve in e.EntityValidationErrors)
                    //{
                    //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    //    foreach (var ve in eve.ValidationErrors)
                    //    {
                    //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                    //            ve.PropertyName, ve.ErrorMessage);
                    //    }
                    //}
                    //throw e;

                }

            } return RedirectToAction("Thankyou");
        }

        public ActionResult List()
        {
            return View();
            //return View(dbContext.JobSeekers.Where(x => x.IsDeleted == false).ToList().OrderByDescending(x => x.CreatedOn));
        }
        public ActionResult Thankyou()
        {
            return View();
        }
        public bool UpdateRating(int Rating, string Feedback, int JobSeekerId, bool IsInterview, bool IsActive)
        {

            var jobSeeker = dbContext.JobSeekers.Where(x => x.Id == JobSeekerId).FirstOrDefault();

            jobSeeker.Rating = Convert.ToInt32(Rating);
            jobSeeker.Feedback = Feedback;
            jobSeeker.IsInterview = IsInterview;
            jobSeeker.IsActive = IsActive;

            dbContext.SaveChanges();

            return true;
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}